﻿using System.Windows;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using Jose;



namespace EPJ
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private void button_Click(object sender, RoutedEventArgs e)
        {

            String practitionerId = "1234512345";

            int selectedItem = peopleListBox.SelectedIndex;
            Console.WriteLine("Selected patient = "+selectedItem);
            String patientIdentifier = "12345";
            if (selectedItem == 0)
            {
                patientIdentifier = "02037340726";
                Console.WriteLine("Was Lars Roland");
            }
            else if (selectedItem == 1)
            {
                patientIdentifier = "02037312345";
                Console.WriteLine("Was Jon MinId");
            }
            else {
                Console.WriteLine("Patient ID not found");
            }

            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var now = Math.Round((DateTime.UtcNow - unixEpoch).TotalSeconds);

            var payload = new Dictionary<string, object>()
            {
                { "exp", now },
                { "sub", practitionerId },
                { "iss", "dummyepj" },
                { "aud", "ehelselab.com" },
                { "jti", Guid.NewGuid().ToString().Substring(0, 8)},
                { "reqpt", patientIdentifier },
                { "reason", "fastlege" }
            };

            X509Certificate2 clientCert = new X509Certificate2("c:\\ehelse\\EPJ\\EPJ\\epjcert.pfx", "secret", X509KeyStorageFlags.Exportable);

            //X509Certificate2 clientCert = new X509Certificate2("d:\\helsedir\\dummyepj\\EPJ\\epjcert.pfx", "secret", X509KeyStorageFlags.Exportable);
            //X509Certificate2 serverCert = new X509Certificate2("d:\\helsedir\\dummyepj\\EPJ\\servercert.pfx", "secret", X509KeyStorageFlags.Exportable);

            // Export public part for use at server to verify
            String clientPublicKeyXML = clientCert.PublicKey.Key.ToXmlString(false);
            Debug.WriteLine("Client certificate's public key = " + clientPublicKeyXML);

            // This instance can not sign and verify with SHA256:
            RSACryptoServiceProvider clientPrivateKey = (RSACryptoServiceProvider)clientCert.PrivateKey;

            // This one can:
            RSACryptoServiceProvider clientPrivateKeyCan = new RSACryptoServiceProvider();
            
            clientPrivateKeyCan.ImportParameters(clientPrivateKey.ExportParameters(true));

            string token = Jose.JWT.Encode(payload, clientPrivateKeyCan, JwsAlgorithm.RS256);

            ///////////////////////
            // Now encrypt

            String publicServerKeyXml = "<RSAKeyValue><Modulus>x9KiSGctpy09rLylwt+R4JMbZWEhVpXvzn7+1p0rjySQeh8PEnoM4Kewunusj4ApBQiYyGhkH1/fFLxTOK2axNySpj0v9nYaSdT2M9vKaIbtMen/LuVa0xDXlKTLWnfWA4geV+XVI4kDh0uPza3ZOBstwzm464kWEC7pTWijS6E=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
            
            //RSACryptoServiceProvider serverPublicKey = serverCert.PublicKey.Key as RSACryptoServiceProvider;
            //Debug.WriteLine("signed tokepublicServerKeyn=" + serverPublicKey.ToXmlString(false));
            RSACryptoServiceProvider serverPublicKey = new RSACryptoServiceProvider();
            serverPublicKey.FromXmlString(publicServerKeyXml);

            string encryptedToken = Jose.JWT.Encode(token, serverPublicKey, JweAlgorithm.RSA_OAEP, JweEncryption.A256GCM);

            Debug.WriteLine("signed token=" + token);
            Debug.WriteLine("signed token length=" + token.Length);

            Console.WriteLine("encryptedToken=" + encryptedToken);
            Debug.WriteLine("encryptedToken length=" + encryptedToken.Length);

            Browser.Address = "http://ehelselabapps.azurewebsites.net/?launch=" + encryptedToken; // + "&unencrypted=" + token;
            //Browser.Address = "http://localhost:63609/?launch=" + encryptedToken; // + "&unencrypted=" + token;

        }
        
        private void peopleListBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Browser.Address = "http://apps.ehelselab.com/client/empty.html";
        }
    }
}
